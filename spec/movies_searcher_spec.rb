require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/movies_searcher"

def when_i_get_the_info_for_the_movie(api_key, movie_title)
  body_the_godfather = { "Response": 'True', "Title": 'The Godfather', "Awards": 'Won 3 Oscars. 31 wins & 31 nominations total' }
  body_titanic = { "Response": 'True', "Title": 'Titanic', "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total' }
  body_emoji = { "Response": 'True', "Title": 'Emoji', "Awards": 'N/A' }
  body_not_found = { "Response": 'False', "Error": 'Movie not found!' }

  body = case movie_title
         when 'The Godfather'
           body_the_godfather
         when 'Titanic'
           body_titanic
         when 'Emoji'
           body_emoji
         else
           body_not_found
         end

  stub_request(:get, "http://www.omdbapi.com/?apikey=#{api_key}&t=#{movie_title}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe MoviesSearcher do
  before(:each) do
    stub_const('ENV', { 'OMDB_API_KEY' => 'fake_key', 'TELEGRAM_TOKEN' => 'fake_token' })
  end

  it 'should return a movie with title "Titanic" and awards "Won 11 Oscars. 126 wins & 83 nominations total"' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Titanic')
    movies_searcher = described_class.new(ENV['OMDB_API_KEY'])
    movies = movies_searcher.search(['Titanic'])

    expect(movies.first.awards).to eq('Won 11 Oscars. 126 wins & 83 nominations total')
  end

  it 'should return a list with two movies, "The Godfather" and "Titanic" with their respective awards' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'The Godfather')
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Titanic')
    movies_searcher = described_class.new(ENV['OMDB_API_KEY'])
    movies = movies_searcher.search(['The Godfather', 'Titanic'])

    expect(movies.first.awards).to eq('Won 3 Oscars. 31 wins & 31 nominations total')
    expect(movies.last.awards).to eq('Won 11 Oscars. 126 wins & 83 nominations total')
  end

  it 'should return a movie with title "Emoji" and awards "This movie has no awards"' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Emoji')
    movies_searcher = described_class.new(ENV['OMDB_API_KEY'])
    movies = movies_searcher.search(['Emoji'])

    expect(movies.first.awards).to eq(nil)
  end

  it 'should return a not a movie with title "Shrek 8" and awards "Movie not found!"' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Shrek 8')
    movies_searcher = described_class.new(ENV['OMDB_API_KEY'])
    movies = movies_searcher.search(['Shrek 8'])

    expect { movies.first.awards }.to raise_error(NoMethodError)
  end
end
