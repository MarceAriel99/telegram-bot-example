require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe 'BotClient' do
  before(:each) do
    stub_const('ENV', { 'OMDB_API_KEY' => 'fake_key', 'TELEGRAM_TOKEN' => 'fake_token' })
  end

  it 'should get a /version message and respond with current version' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/version')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], Version.current)

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/say_hi Emilio')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'Hola, Emilio')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/start')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'Hola, Emilio')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a /stop message and respond with Chau' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/stop')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'Chau, egutter')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/tv')
    then_i_get_keyboard_message(ENV['TELEGRAM_TOKEN'], 'Quien se queda con el trono?')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    when_i_send_keyboard_updates(ENV['TELEGRAM_TOKEN'], 'Quien se queda con el trono?', '2')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'A mi también me encantan los dragones!')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/unknown')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'Uh? No te entiendo! Me repetis la pregunta?')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a "/awards Titanic" message and respond with the awards for Titanic' do
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/awards Titanic')
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Titanic')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'Won 11 Oscars. 126 wins & 83 nominations total')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a "/awards The Godfather;Titanic" message and respond with the awards for The Godfather and Titanic' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'The Godfather')
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Titanic')
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/awards The Godfather;Titanic')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], "Won 3 Oscars. 31 wins & 31 nominations total\nWon 11 Oscars. 126 wins & 83 nominations total")

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a "/awards Emoji" message and respond with a message saying that the movie has no awards' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Emoji')
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/awards Emoji')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'This movie has no awards')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end

  it 'should get a "/awards Shrek 8" message and respond with a message saying that the movie doesn\'\t exist' do
    when_i_get_the_info_for_the_movie(ENV['OMDB_API_KEY'], 'Shrek 8')
    when_i_send_text(ENV['TELEGRAM_TOKEN'], '/awards Shrek 8')
    then_i_get_text(ENV['TELEGRAM_TOKEN'], 'Movie not found!')

    app = BotClient.new(ENV['TELEGRAM_TOKEN'])

    app.run_once
  end
end
