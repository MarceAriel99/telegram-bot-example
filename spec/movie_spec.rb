require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/movie"

describe Movie do
  it 'should return the awards' do
    movie = described_class.new('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')

    expect(movie.awards).to eq('Won 11 Oscars. 126 wins & 83 nominations total')
  end

  it 'should return nil when the movie has no awards' do
    movie = described_class.new('Emoji', 'N/A')

    expect(movie.awards).to eq(nil)
  end
end
