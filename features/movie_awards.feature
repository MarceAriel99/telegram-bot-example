Feature: Movie Awards

  Scenario: I try to get the awards of an existent movie with awards
    Given I send the request to get the awards of the movie "The Godfather"
    When I receive the response
    Then the response should be "Won 3 Oscars. 31 wins & 31 nominations total"

  Scenario: I try to get the awards of two existent movies
    Given I send the request to get the awards of the movie "The Godfather" and "Titanic"
    When I receive the response
    Then the response should be "Won 3 Oscars. 31 wins & 31 nominations total\n
                                Won 11 Oscars. 126 wins & 83 nominations total"

  Scenario: I try to get the awards of an existent movie with no awards
    Given I send the request to get the awards of the movie "Emoji"
    When I receive the response
    Then the response should be "This movie has no awards"

  Scenario: I try to get the awards of a non existent movie
    Given I send the request to get the awards of the movie "Shrek 8"
    When I receive the response
    Then the response should be "Movie not found!"