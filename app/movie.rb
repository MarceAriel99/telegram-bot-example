class Movie
  attr_reader :awards

  def initialize(title, awards)
    @title = title
    @awards = awards == 'N/A' ? nil : awards
  end
end
