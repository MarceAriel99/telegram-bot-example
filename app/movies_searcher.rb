require "#{File.dirname(__FILE__)}/movie"

class MoviesSearcher
  def initialize(omdb_api_key)
    @api_key = omdb_api_key
  end

  def search(movie_titles)
    movie_titles.map do |movie_title|
      response = Faraday.get("http://www.omdbapi.com/?apikey=#{@api_key}&t=#{movie_title}")
      response = JSON.parse(response.body)

      Movie.new(response['Title'], response['Awards']) if response['Response'] == 'True'
    end
  end
end
